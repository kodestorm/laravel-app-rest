<?php

namespace Database\Factories;

use App\Models\AthletesHasRunnings;
use App\Models\Runnings;
use App\Models\RunningsResults;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RunningsResultsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RunningsResults::class;

    private $startTime = [3 => '16:00:00', 5 => '15:00:00', 10 => '14:00:00', 21 => '10:00:00', 42 => '09:00:00'];

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'running_id' => '',
            'athlete_id' => '',
            'time_start' => function (array $attributes) {
                return $this->startTime($attributes['running_id']);
            },
            'time_end' => function (array $attributes) {
                return $this->endTime($attributes['running_id']);
            },
        ];
    }

    private function startTime($runningId)
    {
        return $this->startTime[Runnings::find($runningId)->runningLength->length];
    }

    private function endTime($runningId)
    {
        return date('H:i:s', strtotime(Str::replaceArray('?', [rand(15, 45), rand(00, 59)], '+? minutes +? seconds'), strtotime($this->startTime[Runnings::find($runningId)->runningLength->length])));
    }
}
