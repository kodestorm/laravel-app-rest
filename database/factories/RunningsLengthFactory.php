<?php

namespace Database\Factories;

use App\Models\RunningsLength;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunningsLengthFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RunningsLength::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'length' => rand(10,42)          
        ];
    }
}
