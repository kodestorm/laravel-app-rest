<?php

namespace Database\Factories;

use App\Models\AthletesHasRunnings;
use App\Models\RunningsResults;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class RunningsResultsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = RunningsResults::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {

        // $athletesHasRunnings = AthletesHasRunnings::all();

        // switch ($athletesHasRunnings->running->runningLength->length) {
        //     case 3:
        //         $startTime = '16:00:00';
        //         $endTime = date('H:i:s', strtotime(Str::replaceArray('?', [rand(15, 45), rand(00, 59)], '+? minutes +? seconds'), strtotime($startTime)));
        //         break;
        //     case 5:
        //         $startTime = '15:00:00';
        //         $endTime = date('H:i:s', strtotime(Str::replaceArray('?', [rand(30, 59), rand(00, 59)], '+? minutes +? seconds'), strtotime($startTime)));
        //         break;
        //     case 10:
        //         $startTime = '14:00:00';
        //         $endTime = date('H:i:s', strtotime(Str::replaceArray('?', [rand(50, 70), rand(00, 59)], '+? minutes +? seconds'), strtotime($startTime)));
        //         break;
        //     case 21:
        //         $startTime = '10:00:00';
        //         $endTime = date('H:i:s', strtotime(Str::replaceArray('?', [rand(100, 140), rand(00, 59)], '+? minutes +? seconds'), strtotime($startTime)));
        //         break;
        //     case 42:
        //         $startTime = '09:00:00';
        //         $endTime = date('H:i:s', strtotime(Str::replaceArray('?', [rand(180, 240), rand(00, 59)], '+? minutes +? seconds'), strtotime($startTime)));
        //         break;
        //     default:
        //         # code...
        //         break;
        // }




        return [
            'running_id' => '',
            'athlete_id' => '',
            'running_start' => function (array $attributes) {
                dd($attributes);                
            },
            'running_end' => function (array $attributes) {
                dd('running_start');
            },
        ];

        // return [
        //     'running_id' => $athletesHasRunnings->running->id,
        //     'athlete_id' => $athletesHasRunnings->athlete->id,
        //     'running_start' => $startTime,
        //     'running_end' => $endTime,
        // ];
    }

    // /**
    //  * Configure the model factory.
    //  *
    //  * @return $this
    //  */
    // public function configure()
    // {
    //     return $this->afterMaking(function (User $user) {
    //        dd()
    //     })->afterCreating(function (User $user) {
    //         //
    //     });
    // }
}
