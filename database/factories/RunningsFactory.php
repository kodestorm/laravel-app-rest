<?php

namespace Database\Factories;

use App\Models\Runnings;
use App\Models\RunningsLength;
use Illuminate\Database\Eloquent\Factories\Factory;

class RunningsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Runnings::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'running_length_id' => RunningsLength::all()->random(),
            'date_running' => $this->faker->unique()->dateTimeBetween('2021-10-10', '2021-10-30')
        ];
    }
}
