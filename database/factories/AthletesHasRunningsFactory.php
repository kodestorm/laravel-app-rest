<?php

namespace Database\Factories;

use App\Models\Athletes;
use App\Models\AthletesHasRunnings;
use App\Models\Runnings;
use Illuminate\Database\Eloquent\Factories\Factory;

class AthletesHasRunningsFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = AthletesHasRunnings::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'running_id' => Runnings::all()->random(),
            'athlete_id' => Athletes::all()->random()
        ];
    }
}
