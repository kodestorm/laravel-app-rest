<?php

namespace Database\Factories;

use App\Models\Athletes;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class AthletesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Athletes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->name(),
            'cpf' => Str::replaceArray('?', [rand(111, 999), rand(111, 999), rand(111, 999), rand(11, 99)], '?.?.?-?'),
            'birth_date' => $this->faker->dateTimeBetween('1965-01-01', '2003-12-31')
        ];
    }
}
