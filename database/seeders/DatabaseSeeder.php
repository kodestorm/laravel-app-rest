<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use \App\Models\Athletes;
use App\Models\AthletesHasRunnings;
use \App\Models\Runnings;
use \App\Models\RunningsLength;
use App\Models\RunningsResults;
use Illuminate\Database\Eloquent\Factories\Sequence;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {

        DB::table('athletes_has_runnings')->delete();
        DB::table('runnings_results')->delete();
        DB::table('athletes')->delete();
        DB::table('runnings')->delete();
        DB::table('runnings_lengths')->delete();

        DB::statement("ALTER TABLE athletes AUTO_INCREMENT = 0;");
        DB::statement("ALTER TABLE runnings AUTO_INCREMENT = 0;");
        DB::statement("ALTER TABLE runnings_results AUTO_INCREMENT = 0;");
        DB::statement("ALTER TABLE runnings_lengths AUTO_INCREMENT = 0;");
        DB::statement("ALTER TABLE athletes_has_runnings AUTO_INCREMENT = 0;");

        RunningsLength::factory()->count(5)
            ->state(new Sequence(
                ['length' => '3'],
                ['length' => '5'],
                ['length' => '10'],
                ['length' => '21'],
                ['length' => '42'],
            ))->create();


        $athletes = Athletes::factory(100)->create();

        Runnings::factory()
            ->count(10)
            ->sequence(fn ($sequence) => ['date_running' => date('Y-m-d', strtotime(Str::replaceFirst('?', $sequence->index, "-? day"), strtotime('now')))])
            ->hasAttached($athletes)
            ->create();

        foreach (AthletesHasRunnings::all() as  $value) {
            RunningsResults::factory()->create(['running_id' => $value->running_id, 'athlete_id' => $value->athlete_id]);
        }
    }
}
