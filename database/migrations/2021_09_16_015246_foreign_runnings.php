<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignRunnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('runnings', function (Blueprint $table) {
            $table->foreign('running_length_id', 'fk_running_length')->references('id')->on('runnings_lengths')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('runnings', function (Blueprint $table) {
            $table->dropForeign('fk_running_length');            
        });
    }
}
