<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRunningsResultsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('runnings_results', function (Blueprint $table) {
            $table->id();            
            $table->unsignedBigInteger('running_id');
            $table->unsignedBigInteger('athlete_id');
            $table->time('time_start', $precision = 0);
            $table->time('time_end', $precision = 0);
            $table->timestamps();
            $table->unique(['running_id', 'athlete_id']);
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('runnings_results');
    }
}
