<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class ForeignAthletesHasRunnings extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('athletes_has_runnings', function (Blueprint $table) {            
            $table->foreign('running_id', 'fk_running_has_foreign')->references('id')->on('runnings')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('athlete_id', 'fk_athlete_has_foreign')->references('id')->on('athletes')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('athletes_has_runnings', function (Blueprint $table) {
            $table->dropForeign('fk_running_has_foreign');
            $table->dropForeign('fk_athlete_has_foreign');
        });
    }
}
