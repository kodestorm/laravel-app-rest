<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAthletesHasRunningsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('athletes_has_runnings', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('running_id');
            $table->unsignedBigInteger('athlete_id');
            $table->timestamps();
            $table->unique(['running_id', 'athlete_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('athletes_has_runnings');
    }
}
