## Observações
Foi utilizado Laravel Sail para utililizar Docker, em docker-compose.yml foi alterados as portão padrão do httpd para 8084 
e a porta do maria_db para 3307, para que não conflitem com pas portas do sistema cajo exista uma instalação deles.

Após o inicio o sistema se encontra em http://127.0.0.1:8084/api

## Acessar a pasta do projeto e executar os seguintes comandos:
```bash
 ./vendor/bin/sail up 
 ./vendor/bin/sail php artisan migrate
 ./vendor/bin/sail php artisan db:seed
 ./vendor/bin/sail php artisan test
```

## Documentos de desenvolvimento
Na pasta _dev se encontra o arquivo do Postman, App-Rest-Test.postman_collection.json com todos os endpoits do teste e uma imagem do modelo ER utilizado na elaboração do teste.


