<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Tests\TestCase;

class RunningTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_running()
    {

        $response = $this->postJson('/api/running', ['running_length_id' => 3, 'date_running' => '20/12/2021']);

        if ($response->status() === 201) {
            $response->assertStatus(201)
                ->assertJson([
                    'data' => [
                        'ruunning_Length_id' => 3,
                        'date_running' => '20/12/2021'
                    ]
                ]);
        } else {
            $response->assertStatus(422)
                ->assertJson([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'running_length_id' => [
                            'The running length id has already been taken.'
                        ]
                    ]
                ]);
        }
    }
}
