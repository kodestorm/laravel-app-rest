<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Tests\TestCase;


class AthleteTest extends TestCase
{
    use WithoutMiddleware;

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_athlete()
    {        
        $response = $this->postJson('/api/athlete', ['name' => 'João Jesus', 'cpf' => '262.168.360-64', 'birth_date' => '13/12/2000']);

        if ($response->status() === 201 ) {
            $response->assertStatus(201)
                ->assertJson(['data' => [
                    'name' => 'João Jesus',
                    'cpf' => '262.168.360-64',
                    'birth_date' => '13/12/2000'
                ]]);
        } else {
            $response->assertStatus(422)
                ->assertJson([
                    'message' => 'The given data was invalid.',
                    'errors' => [
                        'cpf' => [
                            'The cpf has already been taken.'
                        ]
                    ]
                ]);
        }
    }
}
