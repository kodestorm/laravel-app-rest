<?php

use App\Http\Controllers\AthleteController;
use App\Http\Controllers\AthleteHasRunningController;
use App\Http\Controllers\RunningController;
use App\Http\Controllers\RunningLengthController;
use App\Http\Controllers\RunningResultController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::apiResources(
    [
        'athlete' => AthleteController::class,
        'running' => RunningController::class,
        'running-result' => RunningResultController::class,
        'running-length' => RunningLengthController::class
    ]
);

Route::apiResource('running.athlete', AthleteHasRunningController::class);
Route::apiResource('running-result.athlete', RunningResultController::class);
Route::get('running-result-general', [RunningResultController::class, 'generalListResults']);


