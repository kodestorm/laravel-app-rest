<?php

namespace App\Http\Controllers;

use App\Http\Resources\AthletesHasRunningsResource;
use App\Models\Athletes;
use App\Models\AthletesHasRunnings;
use App\Models\Runnings;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class AthleteHasRunningController extends Controller
{
    /**
     * Display a listing of the resource.
     * @param   int $idAthlete
     * @return \Illuminate\Http\Response
     */
    public function index($idAthlete)
    {
        dd('sd');
        return AthletesHasRunningsResource::collection(AthletesHasRunnings::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param   int $idAthlete
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, int $idRunning)
    {

        $request->validate([
            'athlete_id' => [
                'required', 'numeric', 'exists:athletes,id',
                Rule::unique('athletes_has_runnings')
                ->where('athlete_id', $request->get('athlete_id'))
                ->where('running_id', $idRunning)
            ],
        ]);

        $running = Runnings::findOrFail($idRunning);
        $athlete = Athletes::findOrFail($request->input('athlete_id'));
        $running->athletes()->save($athlete);        

        if ($running->save()) {
            return new AthletesHasRunningsResource($running);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\AthletesHasRunnings  $athletesHasRunnings
     * @return \Illuminate\Http\Response
     */
    public function show(AthletesHasRunnings $athleteHasRunning)
    {
        return new AthletesHasRunningsResource($athleteHasRunning);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\AthletesHasRunnings  $athleteHasRunning     
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, AthletesHasRunnings $athleteHasRunning)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Runnings  $running
     * @param  int  $idAthlete
     * @return \Illuminate\Http\Response
     */
    public function destroy(Runnings $running, int $idAthlete)
    {
        if ($running->athletes()->detach($idAthlete)) {
            return new AthletesHasRunningsResource($running);
        }
    }
}
