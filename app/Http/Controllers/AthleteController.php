<?php

namespace App\Http\Controllers;

use App\Models\Athletes;
use Illuminate\Http\Request;
use App\Http\Resources\AthletesResource;
use App\Models\AthletesHasRunnings;
use App\Models\RunningsResults;
use Carbon\Carbon;
use Symfony\Component\HttpKernel\Exception\HttpException;

class AthleteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return AthletesResource::collection(Athletes::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'cpf' => 'required|cpf|unique:App\Models\Athletes,cpf',
            'birth_date' => 'required|date_format:d/m/Y|before_or_equal:-18 years'
        ]);

        $athletes = new Athletes;
        $athletes->name = $request->input('name');
        $athletes->cpf = $request->input('cpf');
        $athletes->birth_date = Carbon::createFromFormat('d/m/Y', $request->input('birth_date'))->format('Y-m-d');

        if ($athletes->save()) {
            return new AthletesResource($athletes);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Athletes  $athletes
     * @return \Illuminate\Http\Response
     */
    public function show(Athletes $athlete)
    {
        $athlete = Athletes::findOrFail($athlete->id);
        return new AthletesResource($athlete);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Athletes  $athletes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Athletes $athlete)
    {
        $request->validate([
            'name' => 'required',
            'cpf' => 'required',
            'birth_date' => 'required|date_format:d/m/Y|before_or_equal:-18 years'
        ]);

        $athlete->name = $request->input('name');
        $athlete->cpf = $request->input('cpf');
        $athlete->birth_date = Carbon::createFromFormat('d/m/Y', $request->input('birth_date'))->format('Y-m-d');

        if ($athlete->save()) {
            return new AthletesResource($athlete);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Athletes  $athletes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Athletes $athlete)
    {

        if (RunningsResults::where('athlete_id', $athlete->id)->get() || AthletesHasRunnings::where('athlete_id', $athlete->id)->get()){
            return response()->json(['data' => 'Atleta possui registro em maratonas e resultados de maratonas']);        
        } else {
            if ($athlete->delete()) {
                return new AthletesResource($athlete);
            }
        }
        
    }
}
