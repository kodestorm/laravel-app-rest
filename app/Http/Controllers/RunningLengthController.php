<?php

namespace App\Http\Controllers;

use App\Http\Resources\RunningsLengthResource;
use App\Models\RunningsLength;
use Illuminate\Http\Request;

class RunningLengthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RunningsLengthResource::collection(RunningsLength::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'length' => 'required|numeric',            
        ]);

        $runningsLength = new RunningsLength;
        $runningsLength->length = $request->input('length');        

        if ($runningsLength->save()) {
            return new RunningsLengthResource($runningsLength);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RunningsLength  $runningsLength
     * @return \Illuminate\Http\Response
     */
    public function show(RunningsLength $runningsLength)
    {        
        return new RunningsLengthResource($runningsLength);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RunningsLength  $runningsLength
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RunningsLength $runningsLength)
    {
        $request->validate([
            'length' => 'required|numeric',            
        ]);
        
        $runningsLength->length = $request->input('length');        

        if ($runningsLength->save()) {
            return new RunningsLengthResource($runningsLength);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RunningsLength  $runningsLength
     * @return \Illuminate\Http\Response
     */
    public function destroy(RunningsLength $runningsLength)
    {        
        if ($runningsLength->delete()) {
            return new RunningsLengthResource($runningsLength);
        }
    }
}
