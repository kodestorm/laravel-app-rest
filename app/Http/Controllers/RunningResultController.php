<?php

namespace App\Http\Controllers;

use App\Http\Resources\RunningsResultsResource;
use App\Models\Athletes;
use App\Models\Runnings;
use App\Models\RunningsResults;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RunningResultController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($runningId)
    {
        $runningsResults = new RunningsResults;
        $listResult = collect($runningsResults->listResultByRunning($runningId));
        $listGroupBy = $listResult->groupBy('age_group');
        $listToReturn = collect();

        foreach ($listGroupBy as $groupAge) {
            // percorre a lista agrupada para definir a posição de cada atleta dentro do grupo
            foreach ($groupAge as $key => $athlete) {
                // definindo a posição do atleta pela ordem de menores tempos da consulta
                $athlete->position = $key + 1;                
                $listToReturn->push($athlete);
            }
        }

        return response()->json(['data' => $listToReturn]);        
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function generalListResults()
    {
        $runningsResults = new RunningsResults;
        $listResult = collect($runningsResults->generalListResults());
        $listGroupByRunning = $listResult->groupBy('running_id');        
        $listToReturn = collect();        
        foreach ($listGroupByRunning as $groupRunning) {
            // percorre a lista agrupada para definir a posição de cada atleta dentro da maratona
            foreach ($groupRunning as $key => $athlete) {
                // definindo a posição do atleta pela ordem de menores tempos da consulta
                $athlete->position = $key + 1;                
                $listToReturn->push($athlete);
            }
        }

        return response()->json(['data' => $listToReturn]);        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $idRunning)
    {
        $request->validate([
            'athlete_id' => 'required|numeric|exists:athletes,id',
            'time_end' => 'required|date_format:H:i:s|after:time_start',
            'time_start' => 'required', 'date_format:H:i:s',
            Rule::unique('runnings_results')
                ->where('athlete_id', $request->get('athlete_id'))
                ->where('running_id', $idRunning)

        ]);

        $runningsResults = new RunningsResults;
        $runningsResults->running_id = $idRunning;
        $runningsResults->athlete_id = $request->input('athlete_id');
        $runningsResults->time_start = $request->input('time_start');
        $runningsResults->time_end = $request->input('time_end');

        $runningsResults->save();
        if ($runningsResults->save()) {
            return new RunningsResultsResource($runningsResults);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\RunningsResults  $runningResult
     * @return \Illuminate\Http\Response
     */
    public function show(RunningsResults $runningResult)
    {
        return new RunningsResultsResource($runningResult);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\RunningsResults  $runningResult
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, RunningsResults $runningResult, $idRunning)
    {
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\RunningsResults  $runningResult
     * @return \Illuminate\Http\Response
     */
    public function destroy(RunningsResults $runningResult, $idAthlete)
    {
        if ($runningResult->athletes()->detach($idAthlete)) {
            return true;
        }
    }
}
