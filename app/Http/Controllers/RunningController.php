<?php

namespace App\Http\Controllers;

use App\Http\Resources\RunningsResource;
use App\Models\AthletesHasRunnings;
use App\Models\Runnings;
use App\Models\RunningsResults;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class RunningController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return RunningsResource::collection(Runnings::paginate(15));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'date_running' => 'required|date_format:d/m/Y',
            'running_length_id' => [
                'required', 'numeric', 'exists:runnings_lengths,id',
                Rule::unique('runnings')
                    ->where('running_length_id', $request->get('running_length_id'))
                    ->where('date_running', Carbon::createFromFormat('d/m/Y', $request->input('date_running'))->format('Y-m-d'))
            ]
        ]);

        $running = new Runnings;
        $running->running_length_id = $request->input('running_length_id');
        $running->date_running = Carbon::createFromFormat('d/m/Y', $request->input('date_running'))->format('Y-m-d');

        if ($running->save()) {
            return new RunningsResource($running);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Runnings  $runnings
     * @return \Illuminate\Http\Response
     */
    public function show(Runnings $running)
    {
        // $artigo = Runnings::findOrFail( $id );
        return new RunningsResource($running);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Runnings  $runnings
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Runnings $running)
    {
        $request->validate([
            'date_running' => 'required|date_format:d/m/Y',
            'running_length_id' => [
                'required', 'numeric', 'exists:runnings_lengths,id',
                Rule::unique('runnings')
                    ->where('running_length_id', $request->get('running_length_id'))
                    ->where('date_running', Carbon::createFromFormat('d/m/Y', $request->input('date_running'))->format('Y-m-d'))
                    ->ignore($running->id)
            ]
        ]);

        $running->running_length_id = $request->input('running_length_id');
        $running->date_running = Carbon::createFromFormat('d/m/Y', $request->input('date_running'))->format('Y-m-d');

        if ($running->save()) {
            return new RunningsResource($running);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Runnings  $runnings
     * @return \Illuminate\Http\Response
     */
    public function destroy(Runnings $running)
    {
        if (RunningsResults::where('running_id', $running->id)->get() || AthletesHasRunnings::where('running_id', $running->id)->get()) {
            return response()->json(['data' => 'Maratona possui registro de atletas']);
        } else {
            $running->delete();
            return new RunningsResource($running);
        }
    }
}
