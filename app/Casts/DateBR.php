<?php

namespace App\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsAttributes;

class DateBR implements CastsAttributes
{
    public function get($model, $key, $value, $attributes)
    {        
        if (isset($value) && !empty($value)) {
            return \Carbon\Carbon::parse($value)->format('d/m/Y');
        } 
    }

    public function set($model, $key, $value, $attributes)
    {        
        if (isset($value) && !empty($value)) {
            return \Carbon\Carbon::parse($value)->format('Y/m/d');
        } 
    }
}
