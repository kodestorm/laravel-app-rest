<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Support\Facades\DB;

class RunningsResults extends Model
{
    use HasFactory;
    protected $fillable = ['running_id', 'athlete_id', 'time_start', 'time_end'];
    protected $hidden = ['created_at', 'updated_at'];


    /**
     * The Runnings that belong to the Runnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function athletes(): BelongsToMany
    {
        return $this->belongsToMany(Athletes::class, 'runnings_results', 'running_id', 'athlete_id',);
    }


    public function listResultByRunning($runningId)
    {
        return DB::select("SELECT runnings.id as running_id
                                , runnings_lengths.length as running_length
                                , athletes.id as athlete_id          
                                , TIMESTAMPDIFF(YEAR, athletes.birth_date, NOW()) AS age     
                                , athletes.name                          
                                , (select case when age between 18 and 25 then '18-25'
                                                when age between 26 and 35 then '26-35'
                                                when age between 36 and 45 then '36-45'
                                                when age between 46 and 55 then '46-55'
                                                else '56-99' end as age_group                
                                    ) as age_group             
                                , TIMEDIFF(runnings_results.time_end, runnings_results.time_start) as running_time         
                            FROM athletes  
                            JOIN runnings_results on runnings_results.athlete_id = athletes.id
                            JOIN runnings on runnings.id = runnings_results.running_id          
                            JOIN runnings_lengths on runnings_lengths.id = runnings.running_length_id
                            WHERE running_id = :running_id  
                            ORDER BY running_time ASC, age_group ASC", ['running_id' => $runningId]);    
    }

    public function generalListResults()
    {
        return DB::select("SELECT runnings.id as running_id
                                , runnings_lengths.length as running_type
                                , athletes.id as athlete_id          
                                , athletes.name      
                                , TIMEDIFF(runnings_results.time_end, runnings_results.time_start) as running_time     
                                , TIMESTAMPDIFF(YEAR, athletes.birth_date, NOW()) AS age         
                           FROM athletes  
                           JOIN runnings_results on runnings_results.athlete_id = athletes.id
                           JOIN runnings on runnings.id = runnings_results.running_id          
                           JOIN runnings_lengths on runnings_lengths.id = runnings.running_length_id
                           ORDER BY runnings.id asc, running_time ASC");
    }
}
