<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AthletesHasRunnings extends Model
{
    use HasFactory;
    protected $fillable = ['running_id', 'athlete_id'];
    protected $hidden = ['created_at', 'updated_at'];

    public function athlete()
    {
        return $this->belongsTo(Athletes::class);
        // return $this->belongsTo(RunningsLength::class, 'running_length_id', 'id');
    }

    public function running()
    {
        return $this->belongsTo(Runnings::class);
        // return $this->belongsTo(RunningsLength::class, 'running_length_id', 'id');
    }
}
