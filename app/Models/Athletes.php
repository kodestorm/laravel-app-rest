<?php

namespace App\Models;

use App\Casts\DateBR;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Athletes extends Model
{
    use HasFactory;
    protected $fillable = ['name', 'cpf', 'birth_date'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = ['birth_date'  => DateBR::class];


    /**
     * The Runnings that belong to the Runnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function runnings(): BelongsToMany
    {
        return $this->belongsToMany(Runnings::class, 'athletes_has_runnings', 'athlete_id', 'running_id');
    }

    /**
     * The Runnings that belong to the Runnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function running_results(): BelongsToMany
    {
        return $this->belongsToMany(Athletes::class, 'runnings_results', 'athlete_id', 'running_id');
    }
}
