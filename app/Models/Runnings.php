<?php

namespace App\Models;

use App\Casts\DateBR;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Runnings extends Model
{
    use HasFactory;
    protected $fillable = ['running_length_id', 'date_running'];
    protected $hidden = ['created_at', 'updated_at'];
    protected $casts = ['date_running'  => DateBR::class];


    public function runningLength()
    {
        return $this->belongsTo(RunningsLength::class);
        // return $this->belongsTo(RunningsLength::class, 'running_length_id', 'id');
    }

    /**
     * The Athletes that belong to the Runnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function athletes(): BelongsToMany
    {        
        return $this->belongsToMany(Runnings::class, 'athletes_has_runnings', 'running_id', 'athlete_id');
    }

    /**
     * The RunningResults that belong to the Runnings
     *
     * @return \Illuminate\Database\Eloquent\Relations\BelongsToMany
     */
    public function runnings_results(): BelongsToMany
    {
        return $this->belongsToMany(Athletes::class, 'runnings_results', 'athlete_id','running_id' );
    }
}
