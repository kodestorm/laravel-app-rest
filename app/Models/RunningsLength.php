<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class RunningsLength extends Model
{
    use HasFactory;
    protected $fillable = ['length'];
    protected $hidden = ['created_at','updated_at'];
}
